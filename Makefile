NAME=update
EXEC=$(NAME)

#-----------configure ROOT-----------#
ifdef ROOTSYS
include $(ROOTSYS)/etc/Makefile.arch
ROOTINC     :=$(shell $(ROOTSYS)/bin/root-config --incdir)
ROOTLIBDIR  :=$(shell $(ROOTSYS)/bin/root-config --libdir)
ROOTLDFLAGS :=$(shell $(ROOTSYS)/bin/root-config --ldflags)
ROOTCFLAGS  :=$(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTLIBS    := $(shell  $(ROOTSYS)/bin/root-config --libs)
ROOTLINK = $(ROOTLIBS) $(ROOTCFLAGS) $(ROOTLDFLAGS) -I$(ROOTINC)
else
@echo "NO ROOTSYS!"
endif

#--------configure compiler----------#
CXXFLAGS  += -O2 -Wall -Wno-write-strings
LIBS     = $(ROOTLIBS) -lm -lz -lutil -lnsl -lpthread -lTMVA

.SUFFIXES: .$(SrcSuf) .$(ObjSuf) .$(DllSuf)

App: $(NAME).cpp $(OBJECTS)
	@echo "Comiling App..."
	$(CXX) $(CXXFLAGS) -o $(NAME) $(NAME).cpp $(OBJECTS) $(LIBS)
	@echo "-----------------------------------------------------"

clean:
	@rm -fv $(EXEC)

