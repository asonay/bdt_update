#include "TH1D.h"

#include "MVAExtra.hpp"

using namespace std;

double GetIntegral(TH1 *h){

  double sum=0;
  double dx = (double)(h->GetXaxis()->GetXmax()-h->GetXaxis()->GetXmin())/(double)h->GetNbinsX();
  
  for (int i=0;i<h->GetNbinsX();i++)
    sum+=h->GetBinContent(i+1)*dx;

  return sum;

}

void draw_bdt_test_train(string nj=">=10", string nb=">=4")
{
  string njet = "N_j"+nj;
  string nbjet = "N_bj"+nb;
  string name = njet+"_"+nbjet;
  replace( name.begin(), name.end(), '>', 'g');
  replace( name.begin(), name.end(), '<', 's');
  replace( name.begin(), name.end(), '=', 'e');

  TCut cut = ("("+njet+"&&"+nbjet+"&&BDT_score0_M400_train>-10)*weight_bTagSF_MV2c10_Continuous*weight_mc").c_str();
  TCut cut_tr = ("("+njet+"&&"+nbjet+"&&BDT_score0_M400_test>-10)*weight_bTagSF_MV2c10_Continuous*weight_mc").c_str();

  string files = "/eos/user/a/asonay/HBSM4top_ntuple/mc16a_bdt/mc16a_312440_M400_tttt_1Lge9jge3bj.root";
  string fileb = "/eos/user/a/asonay/HBSM4top_ntuple/mc16a_bdt/mc16a_410470_ttbar_1Lge9jge3bj.root";
  
  double ne=80;double xmin=-1.0;double xmax=1.0;
  string selection1 = "BDT_score0_M400_train";
  string selection2 = "BDT_score0_M400_test";

  TFile *fs = new TFile(files.c_str());
  TFile *fb = new TFile(fileb.c_str());
  TTree *trs = (TTree*)fs->Get("nominal_Loose");
  TTree *trb = (TTree*)fb->Get("nominal_Loose");
  
  TH1D *S_t = new TH1D("S_t","Signal",ne,xmin,xmax);
  TH1D *B_t = new TH1D("B_t","Background",ne,xmin,xmax);
  TH1D *S_tr = new TH1D("S_tr","Signal",ne,xmin,xmax);
  TH1D *B_tr = new TH1D("B_tr","Background",ne,xmin,xmax);
  trs->Project("S_t",selection1.c_str(),cut);
  trb->Project("B_t",selection1.c_str(),cut);
  trs->Project("S_tr",selection2.c_str(),cut_tr);
  trb->Project("B_tr",selection2.c_str(),cut_tr);
  double sfac = ne/(xmax-xmin);
  S_t->Scale(sfac/S_t->Integral());
  B_t->Scale(sfac/B_t->Integral());
  S_tr->Scale(sfac/S_tr->Integral());
  B_tr->Scale(sfac/B_tr->Integral());

  
  double ymin = 1e32;
  for (int i=0;i<ne;i++)
    if (ymin>S_t->GetBinContent(i+1)&&S_t->GetBinContent(i+1)>0.0)
      ymin = S_t->GetBinContent(i+1);
  for (int i=0;i<ne;i++)
    if (ymin>B_t->GetBinContent(i+1)&&B_t->GetBinContent(i+1)>0.0)
      ymin = B_t->GetBinContent(i+1);
  double ymax = -1e32;
  for (int i=0;i<ne;i++)
    if (ymax<S_t->GetBinContent(i+1))
      ymax = S_t->GetBinContent(i+1);
  for (int i=0;i<ne;i++)
    if (ymax<B_t->GetBinContent(i+1))
      ymax = B_t->GetBinContent(i+1);

  TH2F *hcan = new TH2F("hcan","",100,S_t->GetXaxis()->GetXmin(),S_t->GetXaxis()->GetXmax(),100,ymin,1.5*ymax);
  hcan->GetXaxis()->SetTitle("BDT Output");
  hcan->GetYaxis()->SetTitle("1/N (dN/dx)");


  TCanvas *c = new TCanvas("c");
  c->SetTicky(); c->SetTickx();

  S_t->SetFillStyle(1001); S_t->SetFillColor(kAzure+7); S_t->SetLineColor(kAzure+7); S_t->SetLineWidth(3);
  B_t->SetFillStyle(3445); B_t->SetFillColor(kRed-7); B_t->SetLineColor(kRed-7); B_t->SetLineWidth(3);

  S_tr->SetMarkerStyle(20); S_tr->SetMarkerSize(1.7); S_tr->SetMarkerColor(4); S_tr->SetLineColor(4); S_tr->SetLineWidth(3);
  B_tr->SetMarkerStyle(20); B_tr->SetMarkerSize(1.7); B_tr->SetMarkerColor(2); B_tr->SetLineColor(2); B_tr->SetLineWidth(3);

  hcan->Draw();
  S_t->Draw("hist same");
  B_t->Draw("hist same");
  S_tr->Draw("p same");
  B_tr->Draw("p same");
  
  TLegend *l_1 = new TLegend(0.0740375,0.823383,0.537019,0.997512);
  l_1->SetLineWidth(3);
  l_1->SetFillStyle(0);
  l_1->SetHeader("      Training Samples");
  l_1->AddEntry(S_t,("Signal @j"+nj+" bj"+nb).c_str(),"f");
  l_1->AddEntry(B_t,("Background @j"+nj+" bj"+nb).c_str(),"f");
  l_1->Draw();
  TLegend *l_2 = new TLegend(0.538993,0.823383,0.994077,0.997512);
  l_2->SetLineWidth(3);
  l_2->SetFillStyle(0);
  l_2->SetHeader("      Test Samples");//l_2->SetHeader("      Without weighting");
  l_2->AddEntry(S_tr,("Signal @j"+nj+" bj"+nb).c_str(),"p");
  l_2->AddEntry(B_tr,("Background @j"+nj+" bj"+nb).c_str(),"p");
  l_2->Draw();
  c->SaveAs(("plots/bdt0test_train_"+name+".png").c_str());

  //-----------------------------------------------------------

  MVAExtra MVAE("MVAE",S_t,B_t);
  MVAExtra MVAE_tr("MVAE_tr",S_tr,B_tr);

  cout << "Seperation Eval: " << MVAE.GetSeparation()
       << " Seperation Trained: " << MVAE_tr.GetSeparation() << endl;

  //-----------------------------------------------------------

  TCanvas *cg = new TCanvas("cg");
  cg->SetTicky(); cg->SetTickx();

  TH1D *rc_1 = MVAE.GetROC(4);
  TH1D *rc_2 = MVAE_tr.GetROC(4);

  rc_1->SetLineColor(kGray+3); rc_1->SetLineWidth(5); rc_1->SetLineStyle(1);
  rc_2->SetLineColor(kRed-4); rc_2->SetLineWidth(5); rc_2->SetLineStyle(10);

  rc_1->Draw("hist L");
  rc_2->Draw("hist same L");
  
  TLatex latex2;
  latex2.SetTextSize(0.035);
  latex2.SetTextAlign(13);  //align at top
  latex2.DrawLatex(0.4,1.01,"ATLAS #bf{internal}");
 
  TLegend *l_4 = new TLegend(0.11846,0.205224,0.82231,0.376866);
  l_4->SetLineWidth(3);
  l_4->SetFillStyle(0);
  l_4->SetHeader(("ROC Curves @j"+nj+" bj"+nb).c_str());
  l_4->AddEntry(rc_1,Form("Train SEP:  %1.3f",MVAE.GetSeparation()),"l");
  l_4->AddEntry(rc_2,Form("Test SEP:  %1.3f",MVAE_tr.GetSeparation()),"l");
  l_4->Draw();

  
  cg->SaveAs(("plots/roc0test_train_"+name+".png").c_str());

}
