## Setting Up Environment

```
source setup.sh
```

## Compiling

```
make clean;make
```
## Execution

Before executing the file, be ensure variables.txt includes BDT variables in same order in the xml file.

```
./update <Variable name to be recorded> </path/to/ntuple.root> </path/to/weight1.xml>,</path/to/weight2.xml>,.. <cond1>,<cond2>,..
```

Please contact for further detail:
anil.sonay@cern.ch