
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "TSystem.h"
#include "TChain.h"
#include "TCut.h"
#include "TDirectory.h"
#include "TH1F.h"
#include "TH1.h"
#include "TMath.h"
#include "TFile.h"
#include "TStopwatch.h"
#include "TROOT.h"

#include "TMVA/GeneticAlgorithm.h"
#include "TMVA/GeneticFitter.h"
#include "TMVA/IFitterTarget.h"
#include "TMVA/Factory.h"
#include "TMVA/Reader.h"

#include "TTreeReader.h"
#include "TTreeReaderValue.h"


#include "ReadTree.hpp"

using namespace std;
using namespace TMVA;

vector<TString> TcomaSep(string str){
  stringstream ss( str );
  vector<TString> result;

  while( ss.good() )
    {
      string substr;
      getline( ss, substr, ',' );
      result.push_back( substr );
    }

  return result;
}

vector<string> comaSep(string str){
  stringstream ss( str );
  vector<string> result;

  while( ss.good() )
    {
      string substr;
      getline( ss, substr, ',' );
      result.push_back( substr );
    }

  return result;
}

int main(int argc,char **argv)
{

  string ntuple;
  vector<TString> weight_file;
  vector<string> split_cond={"1"};
  string bdt_name = "BDT_score";
  if (argc>1)
    {bdt_name = argv[1];}
  else
    {cout << "Please specify the variable name to be implemented." << endl; return 0;}
  if (argc>2)
    {ntuple = argv[2];}
  else
    {cout << "Please specify the ntuple name." << endl; return 0;}
  if (argc>3)
    {weight_file = TcomaSep(argv[3]);}
  else
    {cout << "Please specify the xml name." << endl; return 0;}
  if (argc>4)
    {split_cond = comaSep(argv[4]);}

  if (weight_file.size() != split_cond.size()){
    cout << "Number of weight file and number of splitting condition must be equal!!" << endl;
    return 0;
  }

  string tree_name = "nominal_Loose";

  // READ INPUTS---------------------
  ifstream in("variables.txt");
  string str;
  if (in.fail()) {cout << "Your variable list is missing!\n"; return 0;}
  vector<string> variables;
  cout << "\nVariables to be filled.. \n" << endl;
  while (in >> str){
    cout << str << endl;
    variables.push_back(str);
  }
  in.close();
  const int variables_size = variables.size();
  
  TFile *f_init = new TFile(ntuple.c_str());
  TTree *tr = (TTree*)f_init->Get(tree_name.c_str());

  ReadTree read("Data",tr,variables);
  cout << "\nFilling..." << endl;
  vector<vector<double>> vars;
  vector<vector<int>> conds;
  for (int i=0;i<read.GetNoE();i++){
    vars.push_back(read.GetInput(i));
    vector<int> cnd;
    for (auto cond : split_cond){
      read.SetSingleVariable(cond);
      cnd.push_back(read.GetInputSingle(i));
    }
    conds.push_back(cnd);
  }
  read.Close();

  if (conds.size() != (unsigned)read.GetNoE()){
    cout << "Total number of event found in condition is not equal to all events in the ntuple. Please check your conditions !! Total Events: " << read.GetNoE() << " Total Events by Condition: " << conds.size() << endl;
    return 0;
  }
  
  //---------------------------------
  //BOOK TMVA------------------------
  TString method = "BDT method";
  vector<TMVA::Reader*> tmva_reader;
  vector<Float_t*> vec_variables;
  for (auto wf : weight_file){
    TMVA::Reader* rdr= new TMVA::Reader( "!Color:!Silent" );
    Float_t *local_variables = new Float_t[variables_size];
    for(int i=0;i<variables_size;i++)
      rdr->AddVariable(variables[i].c_str() , &local_variables[i] );
    vec_variables.push_back(local_variables);
    rdr->BookMVA( method, wf );
    tmva_reader.push_back(rdr);
  }
  //---------------------------------

  //UPDATE TREE----------------------
  cout << "\nUpdating Tree..." << endl;
  TFile *f = new TFile((ntuple).c_str(),"update");
  if (!f) { cout<<"File not found!"<<endl; return 0; }
  TTree *T; f->GetObject(tree_name.c_str(),T);
  if (!T) { cout<<"Tree not found!"<<endl; delete f; return 0; }
  float BDT;
  TBranch *b_BDT = T->Branch(bdt_name.c_str(),&BDT,(bdt_name+"/F").c_str());
  for (int i=0;i<T->GetEntries();i++){

    for (unsigned rsize=0;rsize<tmva_reader.size();rsize++){
      for(int j=0;j<variables_size;j++){
	vec_variables[rsize][j]=vars[i][j];
      }
    }
    BDT=-10;
    for (unsigned rsize=0;rsize<tmva_reader.size();rsize++){
      if (conds[i][rsize])
	BDT=tmva_reader[rsize]->EvaluateMVA( method );
    }
    b_BDT->Fill(); 
  }
  T->Write("nominal_Loose");
  delete f;
  //---------------------------------
  
}
